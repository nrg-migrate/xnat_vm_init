# Environment initialization specific for the XNAT virtual machine configuration.

export JAVA_HOME=/usr/lib/jvm/java-7-oracle
export PATH=~/bin:~/Development/xnat_builder_1_6dev/bin:$PATH

alias md='mkdir -p'
alias rd='rm -r'

