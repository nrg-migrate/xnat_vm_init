Thank you for downloading the XNAT 1.6.3 development virtual machine. This VM was
produced and made available by the XNAT development team in the Neuroinformatics
Research Group at the Washington University School of Medicine.

Update information and more detailed documentation can be found at:

https://wiki.xnat.org/display/XNAT16/XNAT+Virtual+Machine

You can also find out more about XNAT itself at:

http://www.xnat.org

All of the XNAT 1.6.3 virtual machines are available through the NRG FTP site.
These include the following versions:

 * 32-bit Ubuntu Server   ftp://ftp.nrg.wustl.edu/pub/xnat/xnat-1.6.3-32-bit-ubuntu-server.ova
 * 64-bit Ubuntu Server   ftp://ftp.nrg.wustl.edu/pub/xnat/xnat-1.6.3-64-bit-ubuntu-server.ova
 * 32-bit Ubuntu Desktop  ftp://ftp.nrg.wustl.edu/pub/xnat/xnat-1.6.3-32-bit-ubuntu-desktop.ova
 * 64-bit Ubuntu Desktop  ftp://ftp.nrg.wustl.edu/pub/xnat/xnat-1.6.3-64-bit-ubuntu-desktop.ova

MD5 checksums for verifying download validity are available at the same URLs,
with the suffix ".md5".


USER CREDENTIALS
================

If you already know how to set up and configure a virtual machine from the provided
download page, all you need is the default credentials. These are:

Username: xnat
Password: xnat4life

NOTE: We have taken care to make sure that each VM is as up-to-date as possible at
the time that it is exported and uploaded to our FTP site. However, for security
purposes, you should make sure that your VM instance is updated when you first
start it. Also, the default user account and password for the VM are both easily
available and not very secure. You should consider changing the password for your
VM user accounts if there's any chance that the VM may be accessible to anyone
who could exploit the VM's access to your organization's network or the wider
Internet.

Once you've actually started XNAT, you can log into the system with the default
administrator credentials:

Username: admin
Password: admin

There are no other user accounts on the default VM XNAT installation.


REQUIREMENTS
============

The XNAT 1.6.3 development virtual machine was created using VMWare Workstation
and exported in the Open Virtualization Archive (OVA) format. This can be easily
imported into:

 * VMWare Workstation
 * VMWare Player
 * VMWare Fusion (OS X)
 * VMWare vSphere (for deployment to VM clusters)
 * Oracle VirtualBox

You should only run the XNAT virtual machine on a workstation where you can
allocate at least 2 GB of RAM to the running VM instance. 4 GB of RAM is preferred.

NOTE: We have taken care to make sure that each VM is as up-to-date as possible at
the time that it is exported and uploaded to our FTP site. However, for security
purposes, you should make sure that your VM instance is updated when you first
start it. Also, the default user account and password for the VM are both easily
available and not very secure. You should consider changing the password for your
VM user accounts if there's any chance that the VM may be accessible to anyone
who could exploit the VM's access to your organization's network or the wider
Internet.


QUICK START
===========

This presumes that you've already downloaded and extracted the VM files, since
that's where this README comes from.

To get up and running quickly in VMWare products, follow these steps:

1. Select the menu command to open a virtual machine, which is usually just
   File->Open.

2. Browse to the folder where you extracted your VM files.

3. Select the file "XNAT 1.6.3 <Bitness> Ubuntu <Version>.ovf", where <Bitness>
   is either 32-bit or 64-bit and <Version> is Server or Desktop. Click OK and
   wait while your VM tool imports the OVA VM.

4. Once your VM is imported, click start your VM.

5. Once your VM has started up and a login prompt is available, enter the login
   credentials.

6. Once you've logged in, you'll be the XNAT user. You can get root access to
   control, update, and administer your VM by using the sudo command. More
   information on this command is available: http://www.sudo.ws/sudo/intro.html.


ACCESSING XNAT
==============

You can access XNAT via web browser. For the desktop versions of the XNAT 1.6.3
virtual machine, you can use the browser installed on the machine. For both the
desktop and server versions of the machine, you can access XNAT using the browser
on your host machine, but it takes a little bit of set up to work. There are many
different ways you can access XNAT the VM from the host machine depending on how
you set up networking for the VM. This procedure is the simplest but far from
only means. It assumes that you've configured the virtual machine to use network
address translation (or NAT) networking:

1. Log into the VM.

2. Once the command prompt has appeared, type "ifconfig". This will produce
   output something like this:

      xnat@xnatdev:~$ ifconfig
      eth0      Link encap:Ethernet  HWaddr 00:0c:29:43:a5:54  
                inet addr:192.168.139.129  Bcast:192.168.139.255  Mask:255.255.255.0
                inet6 addr: fe80::20c:29ff:fe43:a554/64 Scope:Link
                UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
                RX packets:65 errors:0 dropped:0 overruns:0 frame:0
                TX packets:172 errors:0 dropped:0 overruns:0 carrier:0
                collisions:0 txqueuelen:1000 
                RX bytes:9884 (9.8 KB)  TX bytes:19908 (19.9 KB)
      
      lo        Link encap:Local Loopback  
                inet addr:127.0.0.1  Mask:255.0.0.0
                inet6 addr: ::1/128 Scope:Host
                UP LOOPBACK RUNNING  MTU:16436  Metric:1
                RX packets:8082 errors:0 dropped:0 overruns:0 frame:0
                TX packets:8082 errors:0 dropped:0 overruns:0 carrier:0
                collisions:0 txqueuelen:0 
                RX bytes:1886395 (1.8 MB)  TX bytes:1886395 (1.8 MB)

   Make note of the value for "inet addr:" under the "eth0" entry. In this case,
   the value is "192.168.139.129". This is the IP address for your virtual machine.

3. On your host machine, open the system hosts file. The location of this depends
   on the operating system for your machine. For most Unix-based platforms, i.e.
   Linux and OS X, the file is located at /etc/hosts. For Windows systems, it's
   located in C:\Windows\System32\drivers\etc\hosts. On all platforms, you also
   need to have root or administrator privileges to modify the file.

4. Add a line to your hosts file with the IP address for your VM along with the
   name "xnatdev". This will look something like this:

      192.168.139.129     xnatdev

5. Save the hosts file.

6. In your browser, enter the URL http://xnatdev. XNAT should appear in your
   browser.

7. Log into XNAT using valid credentials (see above for the default credentials
   if you haven't yet set up any user accounts in XNAT).


CONFIGURING XNAT FOR YOUR ENVIRONMENT
=====================================

For the most part, XNAT as configured on your VM is ready to go: you can log in,
create projects, add user accounts, upload imaging data, etc. There are two primary
properties you'll want to change to work properly in your local environment:

 * The mail server setting is set to a dummy value, mail.server. You should change
   this in a couple places:

    - For your deployed XNAT application, change this value in the InstanceSettings.xml
      and services.properties files in the folder /var/lib/tomcat7/webapps/xnat/WEB-INF/conf.
      You'll need to restart Tomcat for these changes to take effect.

    - Change the value for the xdat.mail.server property in your build.properties file.
      This only affects subsequent builds and updates to XNAT.

 * The email addresses for the administrator and all notifications are set to a
   dummy value. To change these, log into XNAT with the admin account and click the
   Administer->Configuration menu command. On the default System tab, you can change
   the administrator email address. On the Notifications tab, you can change email
   address for all of the notifications.


ORGANIZATION
============

The virtual machine uses the following services to deliver XNAT functionality:

 * Tomcat 7 and Apache 2.2 integrated with mod_jk
 * PostgreSQL 9.1
 * XNAT 1.6.3 as available through the standard XNAT download packages

The XNAT source is available in the folder /home/xnat/Development/xnat. The
pipeline engine is located in /home/xnat/Development/pipeline, but is also
linked to /data/xnat/pipeline. The various archive, prearchive, and other data
storage folders are all located in the /data/xnat folder.

You can update XNAT as described in the XNAT documentation:

https://wiki.xnat.org/display/XNAT16/How+to+Upgrade+XNAT

To install modules, such as you might download from http://marketplace.xnat.org,
you can copy the module archive to the folder /data/xnat/modules, then run
through the update procedure as described above. If the installed modules include
new data-type schema definitions, you'll need to make sure you run the database
update script as described in the update steps.


SUPPORT AND HELP
================

There are a number of different forums through which you can get support for
your XNAT virtual machine and other XNAT-related questions:

 * The XNAT discussion group: https://groups.google.com/forum/#!forum/xnat_discussion
 * The XNAT Water Cooler blog and forum site: https://wiki.xnat.org/display/WaterCooler/Home
 * NRG XNAT Twitter feed: https://twitter.com/NrgXnat

