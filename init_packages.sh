#!/bin/bash
#
# (C) 2014 Washington University School of Medicine
# Neuroinformatics Research Group
# Author: Rick Herrick <rick.herrick@wustl.edu>
#
# This script takes a mostly vanilla Ubuntu virtual machine installation and sets it up as an XNAT development
# server. Provisioned services include:
#
#  * The latest XNAT, XDAT core, and pipeline engine
#  * nginx and Tomcat7 integrated via proxy_pass
#  * PostgreSQL 9.1
#  * Provisioned with NeuroDebian (http://neuro.debian.net) for easy installation of open-source neuroimaging tools
# 
# The only prerequisite outside of stock Ubuntu tools required to run this provisioning utility is mercurial.
#
# Remaining features to implement:
#
#  * Adding xnat user to sudoers list for managing PostgreSQL, Tomcat, and Apache services
#  * Configuring PostgreSQL to do cool things.
#  * ...

# If user hasn't sudo'ed, then none of this will work.
if [ "$UID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

read -n1 -r -p "This will initialize this virtual machine with the XNAT development environment. Press Ctrl-C to stop or any other key to continue."

# Initialize mercurial
if [ ! -f ~/.hgrc ]; then
    su -c "cp hgrc ~/.hgrc" -s /bin/bash xnat
fi

apt-get -y install nginx tomcat7 synaptic postgresql-9.1 git vim-gtk
service nginx stop
service tomcat7 stop

mkdir ~/.vim
chmod 700 ~/.vim
chown -R xnat.xnat ~/.vim*

# Set up the default init script. This gets run for all users on login.
cp xnat_init.sh /etc/profile.d/xnat_init.sh
source xnat_init.sh

# Set up graphics
PLATFORM=`uname -m`
if [[ "${PLATFORM}" == "x86_64" ]]; then
    wget ftp://ftp.nrg.wustl.edu/pub/xnat/graphics/XNAT-dtop-163-64b-ubuntu-1920x1200.jpg
else
    wget ftp://ftp.nrg.wustl.edu/pub/xnat/graphics/XNAT-dtop-163-32b-ubuntu-1920x1200.jpg
fi
if [[ ! -d "/home/xnat/Pictures/xnat" ]] ; then mkdir /home/xnat/Pictures/xnat; fi

mv XNAT-dtop*.jpg /home/xnat/Pictures/xnat/background.jpg
chown -R xnat.xnat /home/xnat/Pictures

gsettings set com.canonical.unity-greeter draw-user-backgrounds 'false'
gsettings set com.canonical.unity-greeter background '/home/xnat/Pictures/xnat/background.jpg'
gsettings set org.gnome.desktop.background picture-uri file:///home/xnat/Pictures/xnat/background.jpg

if [[ ! -d "/home/xnat/.gconf/apps/gnome-terminal" ]] ; then mkdir /home/xnat/.gconf/apps/gnome-terminal; fi
cp gconf.xml /home/xnat/.gconf/apps/gnome-terminal/%gconf.xml

cp xnatdev.conf /etc/nginx/sites-available
ln -s /etc/nginx/sites-available/xnatdev.conf /etc/nginx/sites-enabled
rm /etc/nginx/sites-enabled/default

sed -i 's/#\(JAVA_OPTS.*Xrunjdwp.*\)/\1/' /etc/default/tomcat7
sed -i 's/-Xmx128/-Xmx768/' /etc/default/tomcat7
sed -i 's/\(TOMCAT7_USER=\)tomcat7/\1xnat/' /etc/default/tomcat7
sed -i 's/\(TOMCAT7_GROUP=\)tomcat7/\1xnat/' /etc/default/tomcat7
sed -i 's@#JAVA_HOME=.*$@JAVA_HOME=/usr/lib/jvm/java-7-oracle@' /etc/default/tomcat7

python process_server.py /etc/tomcat7/server.xml
mv server.xml /etc/tomcat7/server.xml

chown -RLh xnat.xnat /var/lib/tomcat7
chown -RLh xnat.xnat /etc/tomcat7
chown -RLh xnat.xnat /var/log/tomcat7
chown -RLh xnat.xnat /var/cache/tomcat7

mkdir -p /data/xnat/archive
mkdir -p /data/xnat/build
mkdir -p /data/xnat/cache
mkdir -p /data/xnat/ftp
mkdir -p /data/xnat/modules
mkdir -p /data/xnat/prearchive
mkdir /home/xnat/Development

chown -R xnat.xnat /data /home/xnat/Development

su -c "hg clone https://bitbucket.org/nrg/pipeline_1_6dev /home/xnat/Development/pipeline_1_6dev" -s /bin/bash xnat
su -c "hg clone https://bitbucket.org/nrg/xdat_1_6dev /home/xnat/Development/xdat_1_6dev" -s /bin/bash xnat
su -c "hg clone https://bitbucket.org/nrg/xnat_builder_1_6dev /home/xnat/Development/xnat_builder_1_6dev" -s /bin/bash xnat
pushd /home/xnat/Development/pipeline_1_6dev
su -c "hg update -r 1.6.3" -s /bin/bash xnat
cd /home/xnat/Development/xdat_1_6dev
su -c "hg update -r 1.6.3" -s /bin/bash xnat
cd /home/xnat/Development/xnat_builder_1_6dev
su -c "hg update -r 1.6.3" -s /bin/bash xnat
popd

# Configure the repository to install Java 7
add-apt-repository -y ppa:webupd8team/java
apt-get -y update
apt-get -y install oracle-java7-installer

su -c "psql -U postgres" -s /bin/bash postgres < createdb.sql
su -c "cp build.properties /home/xnat/Development/xnat_builder_1_6dev" -s /bin/bash xnat
su -c "ln -s /home/xnat/Development/pipeline_1_6dev /data/xnat/pipeline" -s /bin/bash xnat

cd /home/xnat/Development/xnat_builder_1_6dev
su -c "cp sample.project .project" -s /bin/bash xnat
su -c "cp sample.classpath .classpath" -s /bin/bash xnat
su -c "bin/setup.sh -Ddeploy=true 2>&1 | tee setup.log" -s /bin/bash xnat

cd deployments/xnat
su -c "psql -U xnat -f sql/xnat.sql" -s /bin/bash xnat
su -c "PATH=/home/xnat/Development/xnat_builder_1_6dev/bin:$PATH; StoreXML -l security/security.xml -allowDataDeletion true" -s /bin/bash xnat
su -c "PATH=/home/xnat/Development/xnat_builder_1_6dev/bin:$PATH; StoreXML -dir ./work/field_groups -u admin -p admin -allowDataDeletion true" -s /bin/bash xnat

# Change the host name
sed -i -r 's/ubuntu/xnatdev/' /etc/hosts
sed -i -r 's/127.0.0.1\s+localhost/127.0.0.1       localhost xnatdev/' /etc/hosts
sed -i -r 's/ubuntu/xnatdev/' /etc/hostname
rm ~/.ICEauthority
service network-manager restart

# Set up neuro.debian.net repos.
wget -O- http://neuro.debian.net/lists/precise.us-tn.libre | tee /etc/apt/sources.list.d/neurodebian.sources.list
apt-key adv --recv-keys --keyserver pgp.mit.edu 2649A5A9
apt-get -y update

# Clean up
apt-get -y autoremove

read -n1 -r -p "Initialization of this virtual machine has been completed. The VM will now be restarted. Press Ctrl-C to stop or any other key to continue."

rm ~/.bash_history
shutdown -r now

