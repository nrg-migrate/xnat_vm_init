#!/usr/bin/python
import sys

replace = open('server.xml.fragment', 'r')
fragment = []
for line in replace:
    fragment.append(line)
replace.close()

incoming = open(sys.argv[1], 'r')
outgoing = open('server.xml', 'w')
found_host = False
replaced_host = False

for line in incoming:
    if not found_host:
        if "<Host" in line:
            found_host = True
            continue
        else:
            outgoing.write(line)
    elif replaced_host:
        outgoing.write(line)
    else:
        if "</Host>" in line:
            outgoing.write("\n".join(fragment))
            replaced_host = True

incoming.close()
outgoing.close()

